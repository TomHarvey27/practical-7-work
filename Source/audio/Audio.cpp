/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    num = 4410;
    
    beep.setFrequency(440.0);
    beep.setAmplitude(1.0);
    beep.setSampleRate(44000);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    

    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    
    while(numSamples--)
    {

        float gain = 0.0;
        
        if (num < 4410)
        {
            num++;
            gain = 1.f;
        }
        
        *outL = beep.nextSample() * gain;
        *outR = *outL;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    beep.setSampleRate(device->getCurrentSampleRate());
    DBG(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}

void Audio::playBeep()
{
    num = 0;
}
