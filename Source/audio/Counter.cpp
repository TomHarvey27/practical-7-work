//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 09/11/2016.
//
//

#include "Counter.hpp"


Counter::Counter(): Thread ("CounterThread")
{
    
    DBG("start thread");
    //startThread();
    
    listener = nullptr;
}


Counter::~Counter()
{
    stopThread(500);
}

void Counter::run()
{
    count = 0;
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
      //  std::cout << "Counter:" << count << "\n";
        if (listener != nullptr)
            listener->counterChanged (count);
        isThreadRunning();
        Time::waitForMillisecondCounter (time + 1000);
        
        count ++;
    }
}



void Counter::toggleButton()
{

    if (isThreadRunning() == false)
    {
     
        startThread();
    }
    
    else
    {
        stopThread(500);
    }
    
}

void Counter::setListener (Listener* newListener)
{
    listener = newListener;
}


