//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Thomas Harvey on 09/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class Counter : public Thread
{
    
public:
    
    
    Counter();
    ~Counter();
    void run() override;
    void toggleButton();
   
    
    class Listener
    {
        
    public:
        
        virtual ~Listener() {}
        
        virtual void counterChanged (const unsigned int counterValue) = 0;
    
    };
    
    void setListener (Listener* newListener);

    
    
private:
    int count = 0;
    Listener* listener;
};


#endif /* Counter_hpp */
