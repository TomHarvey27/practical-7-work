/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for a sinewave oscillator
 */

class SinOscillator  
{
public:
	//==============================================================================
	/**
	 SinOscillator constructor
	 */
	SinOscillator();
	
	/**
	 SinOscillator destructor
	 */
	~SinOscillator();
	
	/**
	 sets the frequency of the oscillator
	 */
	void setFrequency (float freq);
	
	/**
	 sets frequency using a midi note number
	 */
	void setNote (int noteNum);
	
	/**
	 sets the amplitude of the oscillator
	 */
	void setAmplitude (float amp);
	
	/**
	 resets the oscillator
	 */
	void reset();
	
	/**
	 sets the sample rate
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample
	 */
	float nextSample();
	
	/**
	 function that provides the execution of the waveshape
	 */
	float renderWaveShape (const float currentPhase);
	
private:
	float frequency;
    float amplitude;
    float sampleRate;
	float phase;
    float phaseInc;
};

#endif //H_SINOSCILLATOR